//
//  BaseTableView.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//

import Foundation
import UIKit

class BaseTableView:UITableView{
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = ThemeManager.currentTheme().custom_background
        self.separatorStyle = .singleLine
        self.separatorColor = ThemeManager.currentTheme().custom_separator
    }
}
