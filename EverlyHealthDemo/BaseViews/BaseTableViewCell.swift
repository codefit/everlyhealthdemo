//
//  BaseTableViewCell.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//

import UIKit

class BaseTableViewCell:UITableViewCell{
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = ThemeManager.currentTheme().custom_background
        self.selectionStyle = .none
        
    }
}

