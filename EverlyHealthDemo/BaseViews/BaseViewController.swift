//
//  BaseViewController.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = ThemeManager.currentTheme().custom_accent
    }
}
