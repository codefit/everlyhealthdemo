//
//  StringExtensions.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import UIKit

extension String {
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    var withoutSpecialCharacters: String {
        return self.components(separatedBy: CharacterSet.symbols).joined(separator: "").folding(options: .diacriticInsensitive, locale: .current).replacingOccurrences(of: "[^A-Za-z0-9.]", with: "", options: [.regularExpression])
    }

    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    func convertHtml() -> NSMutableAttributedString{
        guard let data = data(using: .isoLatin1) else { return NSMutableAttributedString() }
        do{
            let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
            
            return try NSMutableAttributedString(data: data, options: options, documentAttributes: nil)
        }catch{
            return NSMutableAttributedString()
        }
    }
    func dateWithFormat(withFormat format:String, inputFormat:String)->String{
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "es_CL")
        formatter.dateFormat = format
        formatter.timeZone = .none
        let formatterInput = DateFormatter()
        formatterInput.dateFormat = inputFormat
        
        let yourDate = formatterInput.date(from: self)
        return formatter.string(from: yourDate ?? Date())
    }
    
    func removeAttributes() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.removeAttribute(NSAttributedString.Key.strikethroughStyle, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }

    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }

    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
    
    
}
