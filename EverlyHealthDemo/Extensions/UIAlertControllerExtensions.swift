//
//  UIAlertControllerExtensions.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import UIKit

extension UIAlertController {
    class func show(_ message: String, from controller: UIViewController) {
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default))
        
        if controller is UINavigationController{
            controller.present(alert, animated: true, completion: nil)
        }else{
            controller.show(alert, sender: nil)
        }
    }
}
