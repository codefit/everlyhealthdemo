//
//  Fonts.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.
//

import Foundation

struct Fonts {
    
    //Fonts
    static var FONT_BLACK = "ProximaNova-Black"
    static var FONT_BLACK_IT = "ProximaNova-BlackIt"
    static var FONT_BOLD = "ProximaNova-Bold"
    static var FONT_BOLD_IT = "ProximaNova-BoldIt"
    static var FONT_EXTRA_BOLD = "ProximaNova-Extrabld"
    static var FONT_EXTRA_BOLD_IT = "ProximaNova-ExtrabldIt"
    static var FONT_IT = "ProximaNova-It"
    static var FONT = "ProximaNova-Regular"
    static var FONT_LIGHT = "ProximaNova-Light"
    static var FONT_LIGHT_IT = "ProximaNova-LightIt"
    static var FONT_MEDIUM = "ProximaNova-Medium"
    static var FONT_MEDIUM_IT = "ProximaNova-MediumIt"
    static var FONT_SEMIBOLD = "ProximaNova-Semibold"
    static var FONT_SEMIBOLD_IT = "ProximaNova-SemiboldIt"
    static var FONT_THIN = "ProximaNova-ThinIt"
    static var FONT_THIN_IT = "ProximaNova-ThinIt"
    
}
