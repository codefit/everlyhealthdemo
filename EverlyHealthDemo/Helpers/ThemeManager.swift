//
//  ThemeManager.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.


import Foundation
import UIKit

enum Theme: Int {
    
    case light, dark
    
    // Estructura con variables personalizables

    
    var custom_black: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0x000000)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0xFFFFFF)
        }
    }
    
    var custom_white: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0xFFFFFF)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0x000000)
        }
    }

    var custom_accent: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0x4EA27A)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0x2D3778)
        }
    }
    
    var custom_accent_text: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0x4FA177)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0xFFFFFF)
        }
    }
    
    var custom_background: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0x1B3C3C)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0x2D3778)
        }
    }
    
    var custom_separator: UIColor {
        switch self {
        case .light:
            return custom_accent
        case .dark:
            return custom_accent
        }
    }
    
    
    var custom_button: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0x2D3778)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0x2A6DFF)
        }
    }
    
    var custom_button_text: UIColor {
        switch self {
        case .light:
            return custom_white
        case .dark:
            return custom_black
        }
    }
    var custom_text_primary: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0xBEDBDF)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0xBEDBDF)
        }
    }
    var custom_card: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0xFFFFFF)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0x515151)
        }
    }
    var custom_card_border: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0xffffff)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0x585858)
        }
    }
    
    var custom_text_secondary: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0xFFFFFF)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0xFFFFFF)
        }
    }
    
    var success: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0x8AC926)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0x8AC926)
        }
    }
    
    var success_text: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0x375C65)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0x375C65)
        }
    }
    
    var danger: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0xEF233C)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0xEF233C)
        }
    }
    
    var danger_text: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHex(rgbValue: 0xFFF0F2)
        case .dark:
            return UIColor.colorFromHex(rgbValue: 0xFFF0F2)
        }
    }

    var toolbar: UIColor {
        switch self {
        case .light:
            return custom_accent
        case .dark:
            return custom_accent
        }
    }
    
    var toolbar_text: UIColor {
        switch self {
        case .light:
            return custom_text_primary
        case .dark:
            return custom_text_primary
        }
    }
    
    var toolbar_accent: UIColor {
        switch self {
        case .light:
            return custom_accent
        case .dark:
            return custom_accent
        }
    }

    var barStyle: UIBarStyle {
        switch self {
        case .light:
            return .default
        case .dark:
            return .black
        }
    }
}

// Enum declaration
let SelectedThemeKey = "SelectedTheme"

// This will let you use a theme in the app.
class ThemeManager {
    
    // ThemeManager
    static func currentTheme() -> Theme {
        if let storedTheme = (UserDefaults.standard.value(forKey: SelectedThemeKey) as AnyObject).integerValue {
            return Theme(rawValue: storedTheme)!
        } else {
            return .light
        }
    }
    
    static func applyTheme(theme: Theme) {

        // First persist the selected theme using NSUserDefaults.
        UserDefaults.standard.setValue(theme.rawValue, forKey: SelectedThemeKey)
        UserDefaults.standard.synchronize()
        
        UITabBar.appearance().tintColor = ThemeManager.currentTheme().custom_accent
        
        if #available(iOS 13.0, *) {
            UINavigationBar.appearance().barStyle = theme.barStyle
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.backgroundColor = theme.toolbar
            navBarAppearance.titleTextAttributes = [.foregroundColor: theme.toolbar_text]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: theme.toolbar_text]
            let navigationBarAppearace = UINavigationBar.appearance()
            navigationBarAppearace.tintColor = ThemeManager.currentTheme().custom_text_primary
            navigationBarAppearace.barTintColor = ThemeManager.currentTheme().custom_text_primary
            UINavigationBar.appearance(whenContainedInInstancesOf: [UINavigationController.self]).standardAppearance = navBarAppearance
            UINavigationBar.appearance(whenContainedInInstancesOf: [UINavigationController.self]).scrollEdgeAppearance = navBarAppearance
        } else {
            UINavigationBar.appearance().barStyle = theme.barStyle
            UINavigationBar.appearance().titleTextAttributes =
                [NSAttributedString.Key.foregroundColor: theme.custom_black,
                 NSAttributedString.Key.font: TypoManager.currentTypo().bold(size: 16)]
        }

    }
}

