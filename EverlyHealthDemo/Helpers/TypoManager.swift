//
//  TypoManager.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import UIKit

import Foundation
import UIKit

enum Typo: Int {
    
    case small, normal, big
    
    var multiplier:CGFloat{
        switch self {
        case .small:
            return 0.75
        case .normal:
            return 1
        case .big:
            return 1.3
        }
    }
    
    
    func regular(size:CGFloat)->UIFont{
        return UIFont(name: Fonts.FONT, size: size * multiplier)!
    }
    func bold(size:CGFloat)->UIFont{
        return UIFont(name: Fonts.FONT_BOLD, size: size * multiplier)!
    }
    func semibold(size:CGFloat)->UIFont{
        return UIFont(name: Fonts.FONT_SEMIBOLD, size: size * multiplier)!
    }
    func medium(size:CGFloat)->UIFont{
        return UIFont(name: Fonts.FONT_MEDIUM, size: size * multiplier)!
    }
    func heavy(size:CGFloat)->UIFont{
        return UIFont(name: Fonts.FONT_EXTRA_BOLD, size: size * multiplier)!
    }
    func light(size:CGFloat)->UIFont{
        return UIFont(name: Fonts.FONT_LIGHT, size: size * multiplier)!
    }
    func thin(size:CGFloat)->UIFont{
        return UIFont(name: Fonts.FONT_THIN, size: size * multiplier)!
    }
    func black(size:CGFloat)->UIFont{
        return UIFont(name: Fonts.FONT_BLACK, size: size * multiplier)!
    }
    
    
}

// Enum declaration
let SelectedTypoKey = "SelectedTypo"

// This will let you use a theme in the app.
class TypoManager {
    
    // ThemeManager
    static func currentTypo() -> Typo {
        if let storedTypo = (UserDefaults.standard.value(forKey: SelectedTypoKey) as AnyObject).integerValue {
            return Typo(rawValue: storedTypo)!
        } else {
            return .normal
        }
    }
    
    static func applyTypo(typo: Typo) {
        
        // First persist the selected theme using NSUserDefaults.
        UserDefaults.standard.setValue(typo.rawValue, forKey: SelectedTypoKey)
        UserDefaults.standard.synchronize()
        
        //UIApplication.shared.keyWindow?.rootViewController = FlowController().instantiateViewController(identifier: "main_vc", storyBoard: "Main")

    }
}
