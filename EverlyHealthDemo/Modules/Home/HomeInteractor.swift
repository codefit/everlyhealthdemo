//
//  HomeInteractor.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import Foundation

class HomeInteractor:PTIHomeProtocol{
    
    var presenter: ITPHomeProtocol?
    let postCoreData = CoreDataPosts.init()
    
    func fetchLocalPosts() {
        guard let posts = postCoreData.retrieveData(column: nil, value: nil) else{
            presenter?.didFetchLocalPostsWithError(error: "Error getting local posts")
            return
        }
        presenter?.didFetchLocalPosts(posts: posts as! [Post])
    }
    
    func fetchRemotePosts() {
        PostAPI.getPosts { result in
            switch result{
            case .success(let res):
                self.postCoreData.deleteData(column: nil, value: nil)
                self.postCoreData.insertData(data: res)
                self.presenter?.didFetchRemotePosts(posts: res)
            case .failure(let error):
                self.presenter?.didFetchRemotePostsWithError(error: error.localizedDescription)
            }
        }
    }
}
