//
//  HomeModel.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import Foundation

struct Post: Codable{

    var userId:Int = 0
    var id:Int = 0
    var title:String = ""
    var body:String = ""
    
    enum CodingKeys: String, CodingKey {
    	case userId
        case id
        case title
        case body
    }
}
