//
//  HomeModule.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import UIKit

class HomeModule: IModule {
    let appRouter: IAppRouter
    private var router: HomeRouter!

    init(_ appRouter: IAppRouter) {
        self.appRouter = appRouter
        self.router = HomeRouter(appRouter: self.appRouter)
    }

    func presentView(parameters: [String: Any]) {
        router.presentView(parameters: parameters)
    }

    func createView(parameters: [String: Any]) -> UIViewController? {
        return router.create(parameters: parameters)
    }
}
