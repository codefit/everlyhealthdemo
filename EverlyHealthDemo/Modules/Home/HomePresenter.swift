//
//  HomePresenter.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import Foundation

class HomePresenter:VTPHomeProtocol{
        
    var view: PTVHomeProtocol?
    var interactor: PTIHomeProtocol?
    var router: PTRHomeProtocol?
    var homePosts:[Post]?
    
    init(parameters:[String:Any]) {
        homePosts = parameters["posts"] as? [Post]
    }
    
    func getPosts(){
        interactor?.fetchLocalPosts()
    }
}

extension HomePresenter:ITPHomeProtocol{
    
    func didFetchRemotePosts(posts: [Post]) {
        homePosts = posts
        view?.didGetPosts()
    }
    
    func didFetchRemotePostsWithError(error: String) {
        view?.didGetPostsWithError(error: error)
    }
    
    func didFetchLocalPosts(posts: [Post]) {
        homePosts = posts
        view?.didGetPosts()
        interactor?.fetchRemotePosts()
    }
    
    func didFetchLocalPostsWithError(error: String) {
        interactor?.fetchRemotePosts()
    }
    
}
