//
//  HomeProtocols.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.


import Foundation

protocol VTPHomeProtocol:AnyObject{
    
    var view: PTVHomeProtocol? {get set}
    var interactor: PTIHomeProtocol? {get set}
    var router: PTRHomeProtocol? {get set}
    var homePosts:[Post]? {get set}
    func getPosts()
}

protocol PTVHomeProtocol:AnyObject {
    
    func didGetPosts()
    func didGetPostsWithError(error:String)
}

protocol PTRHomeProtocol:AnyObject {
    func navigateToPostDetail(post:Post?)
}

protocol PTIHomeProtocol:AnyObject {

    var presenter:ITPHomeProtocol? {get set}
    
    func fetchRemotePosts()
    func fetchLocalPosts()
    
    
}

protocol ITPHomeProtocol:AnyObject {
    
    func didFetchRemotePosts(posts:[Post])
    func didFetchRemotePostsWithError(error:String)
    func didFetchLocalPosts(posts:[Post])
    func didFetchLocalPostsWithError(error:String)
}
