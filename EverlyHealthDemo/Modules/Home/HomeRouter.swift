//
//  HomeRouter.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import UIKit

class HomeRouter:PTRHomeProtocol{
    var appRouter: IAppRouter

    init(appRouter: IAppRouter) {
        self.appRouter = appRouter
    }

    func presentView(parameters: [String: Any]) {
        appRouter.presentView(view: create(parameters: parameters))
    }

    func create(parameters: [String: Any]) -> HomeViewController {

        let bundle = Bundle(for: type(of: self))
        let view = HomeViewController(nibName: "HomeViewController", bundle: bundle)        
        let presenter: VTPHomeProtocol & ITPHomeProtocol = HomePresenter(parameters:parameters)
        let interactor: PTIHomeProtocol = HomeInteractor()
        let router:PTRHomeProtocol = HomeRouter(appRouter: appRouter)
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
        
    }
    
    func navigateToPostDetail(post:Post?) {
        AppRouter.share.presentModule(module: ModuleBuilder.postDetail,
                                      parameters: ["post":post as Any],
                                      type: .push)
    }
}
