//
//  HomeViewController.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import UIKit

class HomeViewController: BaseViewController {

    var presenter:VTPHomeProtocol?
    @IBOutlet weak var tv_data: BaseTableView!
    
	override func viewDidLoad() {
        super.viewDidLoad()

        title = NSLocalizedString("home_title", comment: "")
        setTableView()
        presenter?.getPosts()
    }
    
    private func setTableView(){
        tv_data.delegate = self
        tv_data.dataSource = self
        tv_data.rowHeight = UITableView.automaticDimension
        tv_data.estimatedRowHeight = 150
        tv_data.register(PostTVCell.nib, forCellReuseIdentifier: PostTVCell.identifier)
    }
}
extension HomeViewController:PTVHomeProtocol{
    func didGetPosts() {
        tv_data.reloadData()
    }
    
    func didGetPostsWithError(error: String) {
        //Handle error
        //UIAlertController.show(error, from: self.navigationController ?? self)
    }
    
}
extension HomeViewController:UITableViewDelegate, UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.homePosts?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PostTVCell.identifier) as! PostTVCell
        cell.setView(title: presenter?.homePosts?[indexPath.row].title ?? "", body: presenter?.homePosts?[indexPath.row].body ?? "")
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.router?.navigateToPostDetail(post: presenter?.homePosts?[indexPath.row])
    }
}
