//
//  PostTVCell.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import UIKit

class PostTVCell: BaseTableViewCell {

    @IBOutlet weak var lb_body: MediumLabel!
    @IBOutlet weak var lb_title: HugeLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        lb_body.setLineHeight(lineHeight: 1.3)
    }

    func setView(title:String,body:String){
        lb_title.text = title
        lb_body.text = body
    }
}
