//
//  PostCommentTVCell.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import UIKit
import FontAwesome_swift

class PostCommentTVCell: BaseTableViewCell {

    @IBOutlet weak var iv_email: UIImageView!
    @IBOutlet weak var lb_email: MediumLabel!
    @IBOutlet weak var lb_name: LargeLabel!
    @IBOutlet weak var lb_body: SmallLightLabel!
    override func awakeFromNib() {
        super.awakeFromNib()

        lb_body.setLineHeight(lineHeight: 1.3)

        iv_email.image = UIImage.fontAwesomeIcon(name: FontAwesome(rawValue: "fa-envelope")!,
                                                  style: .light,
                                                  textColor: ThemeManager.currentTheme().custom_text_primary,
                                                 size: CGSize(width: 25, height: 25))
    }

    func setView(name:String?, email:String?, body:String?){

        lb_name.text = name
        lb_email.text = email
        lb_body.text = body
    }
}
