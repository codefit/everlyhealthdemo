//
//  PostDetailInfoTVCell.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import UIKit
import FontAwesome_swift

protocol PostDetailInfoTVCellProtocol: AnyObject {
	
}

class PostDetailInfoTVCell: BaseTableViewCell {

    @IBOutlet weak var iv_comments: UIImageView!
    @IBOutlet weak var iv_author: UIImageView!
    @IBOutlet weak var lb_comments: SmallLightLabel!
    @IBOutlet weak var lb_author: SmallLightLabel!
    @IBOutlet weak var lb_body: MediumLabel!
    @IBOutlet weak var lb_title: HugeLabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        iv_author.image = UIImage.fontAwesomeIcon(name: FontAwesome(rawValue: "fa-user")!,
                                                  style: .light,
                                                  textColor: ThemeManager.currentTheme().custom_text_primary,
                                                 size: CGSize(width: 25, height: 25))
        iv_comments.image = UIImage.fontAwesomeIcon(name: FontAwesome(rawValue: "fa-comment")!,
                                                  style: .light,
                                                  textColor: ThemeManager.currentTheme().custom_text_primary,
                                                 size: CGSize(width: 30, height: 30))
    }

    func setView(title:String?,body:String?,author:String?, comments:String?){
        lb_title.text = title
        lb_body.text = body
        
        if let author = author {
            lb_author.text = String(format: NSLocalizedString("posted_by", comment: ""), author)
        }

        if let comments = comments {
            lb_comments.text = String(format: NSLocalizedString("n_of_comments", comment: ""), comments)
        }

    }
}

extension PostDetailInfoTVCell: PostDetailInfoTVCellProtocol {
	
}
