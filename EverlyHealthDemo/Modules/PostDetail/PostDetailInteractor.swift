//
//  PostDetailInteractor.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import Foundation

class PostDetailInteractor:PTIPostDetailProtocol{
    
    let commentCoreData = CoreDataComments.init()
    let userCoreData = CoreDataUsers.init()
    
    var presenter: ITPPostDetailProtocol?
    
    func fetchRemoteComments() {
        CommentAPI.getComments { result in
            switch result{
            case .success(let res):
                self.commentCoreData.deleteData(column: nil, value: nil)
                self.commentCoreData.insertData(data: res)
                self.presenter?.didFetchRemoteComments(comments: res)
            case .failure(let error):
                self.presenter?.didFetchRemoteCommentsWithError(error: error.localizedDescription)
            }
        }
    }
    
    func fetchLocalComments(postId:Int) {
        guard let comments = commentCoreData.retrieveData(column: "postId", value: String(postId)) else{
            presenter?.didFetchLocalCommentsWithError(error: "Error getting local comments")
            return
        }
        presenter?.didFetchLocalComments(comments: comments as! [Comment])
        
    }
    
    func fetchRemoteUsers() {
        UserAPI.getUsers { result in
            switch result{
            case .success(let res):
                self.userCoreData.deleteData(column: nil, value: nil)
                self.userCoreData.insertData(data: res)
                self.presenter?.didFetchRemoteUsers(users: res)
            case .failure(let error):
                self.presenter?.didFetchRemoteUsersWithError(error: error.localizedDescription)
            }
        }
    }
    
    func fetchLocalUsers(userId:Int) {
        guard let users = userCoreData.retrieveData(column: "id", value: String(userId)) else{
            presenter?.didFetchLocalUsersWithError(error: "Error getting local users")
            return
        }
        presenter?.didFetchLocalUsers(users: users as! [User])
    }

}
