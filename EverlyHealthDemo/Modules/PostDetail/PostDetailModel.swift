//
//  PostDetailModel.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import Foundation

struct PostDetail: Codable{

    var post:Post
    var user:User?
    var comments:[Comment]? = []
    
    enum CodingKeys: String, CodingKey {
    	case post
        case user
        case comments
    }
}

struct Comment: Codable{

    var postId:Int
    var id:Int
    var name:String
    var email:String
    var body:String
    enum CodingKeys: String, CodingKey {
        case postId
        case id
        case name
        case email
        case body
    }
}

struct User: Codable{

    var id:Int
    var name:String
    var email:String
    var username:String
    var address:Address? = nil
    var phone:String? = nil
    var website:String? = ""
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case email
        case username
        case address
        case phone
        case website
    }
}

struct Address: Codable{

    var street:String
    var suite:String
    var city:String
    var zipcode:String
    var geo:Geo
    
    enum CodingKeys: String, CodingKey {
        case street
        case suite
        case city
        case zipcode
        case geo
    }
}

struct Geo: Codable{

    var lat:String
    var lng:String
    
    enum CodingKeys: String, CodingKey {
        case lat
        case lng
    }
}

struct Company: Codable{

    var name:String
    var catchPhrase:String
    var bs:String
    
    enum CodingKeys: String, CodingKey {
        case name
        case catchPhrase
        case bs
    }
}
