//
//  PostDetailModule.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import UIKit

class PostDetailModule: IModule {
    let appRouter: IAppRouter
    private var router: PostDetailRouter!

    init(_ appRouter: IAppRouter) {
        self.appRouter = appRouter
        self.router = PostDetailRouter(appRouter: self.appRouter)
    }

    func presentView(parameters: [String: Any]) {
        router.presentView(parameters: parameters)
    }

    func createView(parameters: [String: Any]) -> UIViewController? {
        return router.create(parameters: parameters)
    }
}
