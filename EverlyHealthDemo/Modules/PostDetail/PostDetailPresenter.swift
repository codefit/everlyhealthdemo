//
//  PostDetailPresenter.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import Foundation

class PostDetailPresenter:VTPPostDetailProtocol{
        
    var view: PTVPostDetailProtocol?
    var interactor: PTIPostDetailProtocol?
    var router: PTRPostDetailProtocol?
    var post:Post?
    var postDetail:PostDetail?
    init(parameters:[String:Any]) {
        post = parameters["post"] as? Post
        postDetail = PostDetail(post: post ?? Post(),
                                user: nil,
                                comments: [])
        
    }
    func getComments() {
        interactor?.fetchLocalComments(postId:postDetail?.post.id ?? 0)
    }
    func getUsers() {
        interactor?.fetchLocalUsers(userId: postDetail?.post.userId ?? 0)
    }
}

extension PostDetailPresenter:ITPPostDetailProtocol{
    
    func didFetchLocalComments(comments: [Comment]) {
        postDetail?.comments = comments
        interactor?.fetchRemoteComments()
        view?.didFetchComments()
    }
    
    func didFetchLocalCommentsWithError(error: String) {
        view?.didFetchCommentsWithError(error: error)
    }
    
    func didFetchRemoteComments(comments: [Comment]) {
        let postId = postDetail?.post.id
        postDetail?.comments = comments.filter({$0.postId == postId}).sorted(by: {$0.postId > $1.postId})
        view?.didFetchComments()
    }
    
    func didFetchRemoteCommentsWithError(error: String) {
        view?.didFetchCommentsWithError(error: error)
    }
    
    func didFetchLocalUsers(users: [User]) {
        let userId = postDetail?.post.userId
        postDetail?.user = users.filter({$0.id == userId}).first
        interactor?.fetchRemoteUsers()
        view?.didFetchUsers()
    }
    
    func didFetchLocalUsersWithError(error: String) {
        view?.didFetchUsersWithError(error: error)
    }
    
    func didFetchRemoteUsers(users: [User]) {
        let userId = postDetail?.post.userId
        postDetail?.user = users.filter({$0.id == userId}).first
        view?.didFetchUsers()
    }
    
    func didFetchRemoteUsersWithError(error: String) {
        view?.didFetchUsersWithError(error: error)
    }
}
