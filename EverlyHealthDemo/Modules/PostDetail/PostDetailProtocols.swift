//
//  PostDetailProtocols.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.


import Foundation

protocol VTPPostDetailProtocol:AnyObject{
    
    var view: PTVPostDetailProtocol? {get set}
    var interactor: PTIPostDetailProtocol? {get set}
    var router: PTRPostDetailProtocol? {get set}
    var post:Post? {get set}
    var postDetail:PostDetail? {get set}
    func getComments()
    func getUsers()
}

protocol PTVPostDetailProtocol:AnyObject {

    func didFetchComments()
    func didFetchCommentsWithError(error:String)
    func didFetchUsers()
    func didFetchUsersWithError(error:String)
}

protocol PTRPostDetailProtocol:AnyObject {
    
}

protocol PTIPostDetailProtocol:AnyObject {

    var presenter:ITPPostDetailProtocol? {get set}
    
    func fetchRemoteComments()
    func fetchLocalComments(postId:Int)
    
    func fetchRemoteUsers()
    func fetchLocalUsers(userId:Int)
    
}

protocol ITPPostDetailProtocol:AnyObject {

    func didFetchLocalComments(comments:[Comment])
    func didFetchLocalCommentsWithError(error:String)
    func didFetchRemoteComments(comments:[Comment])
    func didFetchRemoteCommentsWithError(error:String)
    
    func didFetchLocalUsers(users:[User])
    func didFetchLocalUsersWithError(error:String)
    func didFetchRemoteUsers(users:[User])
    func didFetchRemoteUsersWithError(error:String)
    
    
}
