//
//  PostDetailViewController.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import UIKit

class PostDetailViewController: BaseViewController {

    var presenter:VTPPostDetailProtocol?
    @IBOutlet weak var tv_data: BaseTableView!
    
	override func viewDidLoad() {
        super.viewDidLoad()

        setTableView()
        presenter?.getComments()
        presenter?.getUsers()
    }
    
    private func setTableView(){
        tv_data.delegate = self
        tv_data.dataSource = self
        tv_data.estimatedRowHeight = 150
        tv_data.separatorStyle = .none
        tv_data.rowHeight = UITableView.automaticDimension
        tv_data.register(PostDetailInfoTVCell.nib, forCellReuseIdentifier: PostDetailInfoTVCell.identifier)
        tv_data.register(PostCommentTVCell.nib, forCellReuseIdentifier: PostCommentTVCell.identifier)
    }
}
extension PostDetailViewController:PTVPostDetailProtocol{
    
    func didFetchCommentsWithError(error: String) {
        //Handle error
    }
    
    func didFetchUsersWithError(error: String) {
        //Handle error
    }
    
    func didFetchUsers() {
        tv_data.reloadData()
    }
    func didFetchComments() {
        tv_data.reloadData()
    }
}
extension PostDetailViewController:UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case 0:
            return 1
        case 1:
            return presenter?.postDetail?.comments?.count ?? 0
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section{
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: PostDetailInfoTVCell.identifier) as! PostDetailInfoTVCell
            cell.setView(title: presenter?.postDetail?.post.title,
                         body: presenter?.postDetail?.post.body,
                         author: presenter?.postDetail?.user?.username, comments: String(presenter?.postDetail?.comments?.count ?? 0))
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: PostCommentTVCell.identifier) as! PostCommentTVCell
            cell.setView(name: presenter?.postDetail?.comments?[indexPath.row].name,
                         email: presenter?.postDetail?.comments?[indexPath.row].email,
                         body: presenter?.postDetail?.comments?[indexPath.row].body)
            return cell
        default:
            return UITableViewCell()
        }
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}
