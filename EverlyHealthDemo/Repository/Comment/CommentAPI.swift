//
//  CommentAPI.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.
import Foundation
import Alamofire


class CommentAPI {
    
    static func getComments(completion:@escaping (Result<[Comment],AFError>)->Void){
        AlamofireSession.session.request(CommentEndpoint.getComments)
            .validate()
            .responseDecodable { (response: DataResponse<[Comment], AFError>) in
                //print(response.debugDescription)
                completion(response.result)
            }
    }
}
