//
//  CommentEndpoint.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import Foundation
import Alamofire


struct CommentRoutes{
    static let commentsAPI = "comments"
}

enum CommentEndpoint: APIConfiguration {

    case getComments

    // MARK: - HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .getComments:
            return .get
        }
    }
    
    // MARK: - Path
    var path: String {
        switch self {
        case .getComments:
            return "\(CommentRoutes.commentsAPI)"
        }
    }
    
    // MARK: - Parameters
    var parameters: Parameters? {
        switch self {
        case .getComments:
            return nil
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        
        let url = try Endpoints.Environment.baseURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .getComments:

            urlRequest.httpMethod = method.rawValue
            return urlRequest
        }
    }
}
