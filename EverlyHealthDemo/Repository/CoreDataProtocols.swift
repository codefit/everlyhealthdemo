//
//  CoreDataProtocols.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 26-01-22.
//
import Foundation
import UIKit
import CoreData


protocol CoreDataProtocols {
    
    func insertData(data:[Any])
    func retrieveData(column:String?, value:String?)->[Any]?
    func updateData(data: Any, column:String, value:String)
    func deleteData(column:String?, value:String?)
}
