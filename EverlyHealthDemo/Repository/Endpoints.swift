//
//  Endpoints.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import Foundation

struct Endpoints {
    
    struct Environment {
        
        static let baseEnvironment = Bundle.main.object(forInfoDictionaryKey: "BaseURL") as! String
        static let baseURL =  baseEnvironment
        
    }

}
