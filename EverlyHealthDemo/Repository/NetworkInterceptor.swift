//
//  NetworkInterceptor.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.
import Foundation
import Alamofire

class NetworkInterceptor: RequestInterceptor {

    // MARK: - RequestAdapter
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        
        var adaptedRequest = urlRequest
        //adaptedRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        //adaptedRequest.setValue("Bearer " + "", forHTTPHeaderField: HTTPHeaderField.authentication.rawValue)
        completion(.success(adaptedRequest))
    }


    // MARK: - RequestRetrier
    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        print("retrying")
        if let response = request.task?.response as? HTTPURLResponse,

           response.statusCode == 401{
            
            print("ERROR 401 \(response.url?.absoluteString)")
            DispatchQueue.main.async {
                
            }

        } else {
            completion(.doNotRetry)
        }
    }
}

