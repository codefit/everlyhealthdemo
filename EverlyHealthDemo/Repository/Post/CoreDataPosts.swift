//
//  CoreDataPosts.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 26-01-22.
//

import Foundation
import CoreData
import UIKit

class CoreDataPosts:CoreDataProtocols{
    
    static let entity = "EPost"
    
    func insertData(data: [Any]) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: CoreDataPosts.entity, in: managedContext)!
        
        for i in data {
            
            let newData = i as? Post
            let object = NSManagedObject(entity: entity, insertInto: managedContext)
            object.setValue(newData?.userId, forKeyPath: "userId")
            object.setValue(newData?.id, forKey: "id")
            object.setValue(newData?.title, forKey: "title")
            object.setValue(newData?.body, forKey: "body")
        }
                
        do {
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func retrieveData(column:String?, value:String?) -> [Any]? {
        
        var objects:[Post] = []
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil}
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPosts.entity)
        
        if column != nil && value != nil{
            fetchRequest.predicate = NSPredicate(format: "\(column!) = %@", "\(value!)")
        }
        //        fetchRequest.fetchLimit = 1
        //        fetchRequest.sortDescriptors = [NSSortDescriptor.init(key: "email", ascending: false)]
        //
        do {
            let result = try managedContext.fetch(fetchRequest)
            for data in result as! [NSManagedObject] {
                let newObject = Post(userId: data.value(forKey: "userId") as! Int,
                                     id: data.value(forKey: "id") as! Int,
                                     title: data.value(forKey: "title") as! String,
                                     body: data.value(forKey: "body") as! String)
                
                objects.append(newObject)
            }
            
        }catch{
            print("Failed to retrieve data")
            return nil
            
        }
        return objects
    }
    
    func updateData(data: Any, column:String, value:String) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: CoreDataPosts.entity)
        fetchRequest.predicate = NSPredicate(format: "\(column) = %@", "\(value)")
        do{
            let test = try managedContext.fetch(fetchRequest)
            let post = data as? Post
            let objectUpdate = test[0] as! NSManagedObject
            objectUpdate.setValue(post?.userId, forKey: "userId")
            objectUpdate.setValue(post?.id, forKey: "id")
            objectUpdate.setValue(post?.title, forKey: "title")
            objectUpdate.setValue(post?.body, forKey: "body")
            do{
                try managedContext.save()
            }
            catch{
                print(error)
            }
        }
        catch{
            print(error)
        }
    }
    
    func deleteData(column:String?, value:String?) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataPosts.entity)
        
        if column != nil && value != nil{
            fetchRequest.predicate = NSPredicate(format: "\(column!) = %@", "\(value!)")
        }
        
        do{
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results{
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedContext.delete(managedObjectData)
            }
            do{
                try managedContext.save()
            }
            catch
            {
                print(error)
            }
            
        }catch{
            print(error)
        }
    }
}
