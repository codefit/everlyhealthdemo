//
//  PostAPI.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.
import Foundation
import Alamofire


class PostAPI {
    
    static func getPosts(completion:@escaping (Result<[Post],AFError>)->Void){
        AlamofireSession.session.request(PostEndpoint.getPosts)
            .validate()
            .responseDecodable { (response: DataResponse<[Post], AFError>) in
                print(response.debugDescription)
                completion(response.result)
                
                
            }
    }
}
