//
//  PostEndpoint.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import Foundation
import Alamofire


struct PostRoutes{
    static let postsAPI = "posts"

}

enum PostEndpoint: APIConfiguration {

    case getPosts

    // MARK: - HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .getPosts:
            return .get
        }
    }
    
    // MARK: - Path
    var path: String {
        switch self {
        case .getPosts:
            return "\(PostRoutes.postsAPI)"
        }
    }
    
    // MARK: - Parameters
    var parameters: Parameters? {
        switch self {
        case .getPosts:
            return nil
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        
        let url = try Endpoints.Environment.baseURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .getPosts:
            urlRequest.httpMethod = method.rawValue
            return urlRequest
        }
    }
}
