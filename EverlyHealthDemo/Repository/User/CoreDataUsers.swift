//
//  CoreDataUsers.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 26-01-22.
//


import Foundation
import CoreData
import UIKit

class CoreDataUsers:CoreDataProtocols{
    
    static let entity = "EUser"
    
    func insertData(data: [Any]) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: CoreDataUsers.entity, in: managedContext)!
        
        for i in data {
            
            let newData = i as? User
            let object = NSManagedObject(entity: entity, insertInto: managedContext)
            object.setValue(newData?.id, forKey: "id")
            object.setValue(newData?.name, forKey: "name")
            object.setValue(newData?.email, forKey: "email")
            object.setValue(newData?.username, forKey: "username")
        }
        
        //Now we have set all the values. The next step is to save them inside the Core Data
        
        do {
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func retrieveData(column:String?, value:String?) -> [Any]? {
        
        var objects:[User] = []
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil}
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataUsers.entity)
        
        if column != nil && value != nil{
            fetchRequest.predicate = NSPredicate(format: "\(column!) = %@", "\(value!)")
        }
        //        fetchRequest.fetchLimit = 1
        //        fetchRequest.sortDescriptors = [NSSortDescriptor.init(key: "email", ascending: false)]
        //
        do {
            let result = try managedContext.fetch(fetchRequest)
            for data in result as! [NSManagedObject] {
                let newObject = User(id: data.value(forKey: "id") as! Int,
                                    name: data.value(forKey: "name") as! String,
                                    email: data.value(forKey: "email") as! String,
                                    username: data.value(forKey: "username") as! String)
                objects.append(newObject)
            }
            
        }catch{
            print("Failed to retrieve data")
            return nil
            
        }
        return objects
    }
    
    func updateData(data: Any, column:String, value:String) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: CoreDataUsers.entity)
        fetchRequest.predicate = NSPredicate(format: "\(column) = %@", "\(value)")
        do{
            let test = try managedContext.fetch(fetchRequest)
            let obj = data as? User
            let objectUpdate = test[0] as! NSManagedObject
            objectUpdate.setValue(obj?.id, forKey: "id")
            objectUpdate.setValue(obj?.name, forKey: "name")
            objectUpdate.setValue(obj?.email, forKey: "email")
            objectUpdate.setValue(obj?.username, forKey: "username")
            do{
                try managedContext.save()
            }
            catch{
                print(error)
            }
        }
        catch{
            print(error)
        }
    }
    
    func deleteData(column:String?, value:String?) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataUsers.entity)
        
        if column != nil && value != nil{
            fetchRequest.predicate = NSPredicate(format: "\(column!) = %@", "\(value!)")
        }
        
        do{
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results{
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedContext.delete(managedObjectData)
            }
            do{
                try managedContext.save()
            }
            catch
            {
                print(error)
            }
            
        }catch{
            print(error)
        }
    }
}

