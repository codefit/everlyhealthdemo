//
//  UserAPI.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import Foundation
import Alamofire


class UserAPI {
    
    static func getUsers(completion:@escaping (Result<[User],AFError>)->Void){
        AlamofireSession.session.request(UserEndpoint.getUsers)
            .validate()
            .responseDecodable { (response: DataResponse<[User], AFError>) in
                //print(response.debugDescription)
                completion(response.result)
            }
    }
}
