//
//  UserEndpoint.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import Foundation
import Alamofire


struct UserRoutes{
    static let usersAPI = "users"
}

enum UserEndpoint: APIConfiguration {

    case getUsers

    // MARK: - HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .getUsers:
            return .get
        }
    }
    
    // MARK: - Path
    var path: String {
        switch self {
        case .getUsers:
            return "\(UserRoutes.usersAPI)"
        }
    }
    
    // MARK: - Parameters
    var parameters: Parameters? {
        switch self {
        case .getUsers:
            return nil
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try Endpoints.Environment.baseURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        switch self {
        case .getUsers:
            urlRequest.httpMethod = method.rawValue
            return urlRequest
        }

    }
}
