//
//  ButtonStyles.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import UIKit

class RoundedButton : BaseButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.layer.cornerRadius = self.frame.size.height / 3
        self.layer.masksToBounds = true
        self.backgroundColor = ThemeManager.currentTheme().custom_button
        self.setTitleColor(ThemeManager.currentTheme().custom_button_text, for: .normal)
        self.titleLabel?.font = TypoManager.currentTypo().semibold(size: 16)
    }
}

class RoundedCancelButton : RoundedButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {

        self.backgroundColor = ThemeManager.currentTheme().custom_button
        self.setTitleColor(ThemeManager.currentTheme().custom_white, for: .normal)
        self.titleLabel?.font = TypoManager.currentTypo().semibold(size: 16)
    }
}

class RoundedOutlinedButton : RoundedButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {

        self.backgroundColor = .clear
        self.layer.borderWidth = 1
        self.layer.borderColor = ThemeManager.currentTheme().custom_text_primary.cgColor
        self.setTitleColor(ThemeManager.currentTheme().custom_text_primary, for: .normal)
        self.titleLabel?.font = TypoManager.currentTypo().semibold(size: 16)
    }
}

class BaseButton : UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func startAnimatingPressActions() {
        addTarget(self, action: #selector(animateDown), for: [.touchDown, .touchDragEnter])
        addTarget(self, action: #selector(animateUp), for: [.touchDragExit, .touchCancel, .touchUpInside, .touchUpOutside])
    }
    private func setup() {
        startAnimatingPressActions()
    }
    @objc private func animateDown(sender: UIButton) {
        animate(sender, transform: CGAffineTransform.identity.scaledBy(x: 0.95, y: 0.95))
    }
    
    @objc private func animateUp(sender: UIButton) {
        animate(sender, transform: .identity)
    }
    private func animate(_ button: UIButton, transform: CGAffineTransform) {
        UIView.animate(withDuration: 0.4,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 3,
                       options: [.curveEaseInOut],
                       animations: {
                        button.transform = transform
                       }, completion: nil)
    }
}
