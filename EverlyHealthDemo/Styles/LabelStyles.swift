//
//  LabelStyles.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import UIKit

class SmallLightLabel : UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {

        self.textColor = ThemeManager.currentTheme().custom_text_secondary
        self.font = TypoManager.currentTypo().light(size: 12)
    }
    
}
class SmallRegularLabel : UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {

        self.textColor = ThemeManager.currentTheme().custom_text_primary
        self.font = TypoManager.currentTypo().regular(size: 13)
    }
    
}

class MediumLabel : UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {

        self.textColor = ThemeManager.currentTheme().custom_text_secondary
        self.font = TypoManager.currentTypo().regular(size: 16)
    }
    
}

class LargeLabel : UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {

        self.textColor = ThemeManager.currentTheme().custom_text_secondary
        self.font = TypoManager.currentTypo().medium(size: 20)
    }
    
}

class HugeLabel : UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {

        self.textColor = ThemeManager.currentTheme().custom_text_primary
        self.font = TypoManager.currentTypo().bold(size: 28)
    }
    
}

class ExtraHugeLabel : UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {

        self.textColor = ThemeManager.currentTheme().custom_text_primary
        self.font = TypoManager.currentTypo().bold(size: 42)
    }
    
}


class TagLabel : UILabel {
    
    @IBInspectable var topInset: CGFloat = 2.5
    @IBInspectable var bottomInset: CGFloat = 2.5
    @IBInspectable var leftInset: CGFloat = 12.0
    @IBInspectable var rightInset: CGFloat = 12.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {

        self.font = TypoManager.currentTypo().regular(size: 14)
        self.layer.cornerRadius = self.frame.size.height / 2
        self.layer.masksToBounds = true
        self.layer.borderColor = ThemeManager.currentTheme().custom_accent.cgColor
        self.layer.borderWidth = 1
        setSelected(false)
    }

    func setSelected(_ selected:Bool){
        if selected{
            self.backgroundColor = ThemeManager.currentTheme().custom_accent_text
            self.textColor = ThemeManager.currentTheme().custom_white
        }else{
            self.backgroundColor = .clear
            self.textColor = ThemeManager.currentTheme().custom_text_primary
        }

    }
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
        setup()
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
}

