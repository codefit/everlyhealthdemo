//
//  ViewStyles.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import UIKit

class CardView : UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        
        self.backgroundColor = .clear
        self.layer.cornerRadius = 6
        self.layer.borderColor = ThemeManager.currentTheme().custom_separator.withAlphaComponent(0.3).cgColor
        self.layer.borderWidth = 1
        self.dropShadow(color: ThemeManager.currentTheme().custom_black,
                        opacity:0.1,
                        offSet: CGSize(width: 0,
                                       height: 0),
                        radius:2)
    }
}

class SeparatorView : UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.alpha = 0.5
        self.backgroundColor = ThemeManager.currentTheme().custom_black.withAlphaComponent(0.1)
    }
}

@IBDesignable
class GradientView: UIView{
    
    @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
    
    override class var layerClass: AnyClass { return CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors    = [startColor.cgColor, endColor.cgColor]
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
    }
}
class CardGradientView : GradientView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        
        diagonalMode = true
        startColor = ThemeManager.currentTheme().custom_background
        endColor = ThemeManager.currentTheme().custom_accent.withAlphaComponent(0.2)
        startLocation = 0
        endLocation = 0.6

        self.backgroundColor = ThemeManager.currentTheme().custom_card
        self.layer.cornerRadius = 14
        self.dropShadow(color: ThemeManager.currentTheme().custom_black, opacity:0.3, offSet: CGSize(width: 0, height: 0), radius:1)
    }
}
class TransparentCardGradientView : GradientView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        
        diagonalMode = true
        startColor = .clear
        endColor = ThemeManager.currentTheme().custom_accent.withAlphaComponent(0.2)
        startLocation = 0
        endLocation = 0.6

        self.backgroundColor = .clear
        self.layer.cornerRadius = 14
        self.dropShadow(color: ThemeManager.currentTheme().custom_black, opacity:0.3, offSet: CGSize(width: 0, height: 0), radius:1)
    }
}

