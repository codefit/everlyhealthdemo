//
//  EverlyHealthDemo.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//  Copyright (c) 2022 Gianni Murillo Anziani. All rights reserved.

import Foundation

public enum ModuleBuilder: Module {
    case home
    case postDetail

    public var routePath: String {
        switch self {
        case .home:
            return "EverlyHealthDemo/Home"
        case .postDetail:
            return "EverlyHealthDemo/PostDetail"
        }
    }
}
