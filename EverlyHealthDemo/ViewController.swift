//
//  ViewController.swift
//  EverlyHealthDemo
//
//  Created by gianni murillo anziani on 25-01-22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lb_title: ExtraHugeLabel!
    @IBOutlet weak var btn_continue: RoundedButton!
    @IBOutlet weak var lb_message: LargeLabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        lb_title.text = NSLocalizedString("welcome_title", comment: "")
        lb_message.text = NSLocalizedString("welcome_message", comment: "")
        btn_continue.setTitle(NSLocalizedString("enter_demo", comment: ""), for: .normal)
        
        lb_title.animateFromBottom(duration: 1.2, delay: 0.2, offset: 40)
        lb_message.animateFromBottom(duration: 1.2, delay: 0.4, offset: 40)
        btn_continue.animateFromBottom(duration: 0.7, delay: 1.2, offset: 70)
    }
    
    @IBAction func btn_continueTapped(_ sender: Any) {
        AppRouter.share.presentModule(module: ModuleBuilder.home, type: .root)
    }
}
