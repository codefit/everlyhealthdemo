# README #

This is a test project for EverlyHealth

### What is implemented ###

* Post, User, Comment models
* CoreData
* Offline support

### Some comments ###

* Developed under the VIPER architecture
* .xcdatamodel does not implement relationships between tables due the simplicity of this project
